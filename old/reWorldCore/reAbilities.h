#pragma once
#include <sstream>
namespace reWorldCore
{
	namespace reAbility
	{
		enum Ability
		{
			Strength,
			Dexterity,
			Constitution,
			Intelligence,
			Wisdom,
			Charisma,
			Count
		};
		const char* AbilityNames[]=
		{
			"Strength",
			"Dexterity",
			"Constitution",
			"Intelligence",
			"Wisdom",
			"Charisma",
		};
		const char* AbilityNamesShort[]=
		{
			"STR",
			"DEX",
			"CON",
			"INT",
			"WIS",
			"CHA",
		};
		int getIndexFromName(std::string name)
		{
			for(int i=0; i<Count; ++i)
			{
				if(name == AbilityNames[i])
				{
					return i;
				}
			}
			return (int)Count;
		}
		int getIndexFromShortName(std::string name)
		{
			for(int i=0; i<Count; ++i)
			{
				if(name == AbilityNamesShort[i])
				{
					return i;
				}
			}
			return (int)Count;
		}
	}

	class reAbilities
	{
	private:
		unsigned int mAbilities[reAbility::Count];
	public:
		reAbilities(void);
		~reAbilities(void);
		void printAbilities(std::ostream ostr) const;
		int getAbility(int index) const;
		int getModifier(int index) const;
	};

}