#pragma once
#include "reXMLParser.h"
#include "reStringTools.h"
#include "reSingleton.h"
#include <vector>

namespace reWorldCore
{
	class reLanguage
	{
	friend class reLanguageManager;
	private:
		std::string mName;
		std::string mClass;
		void parseXML(const reXMLNode* node);
	public:
		reLanguage(void);
		~reLanguage(void);
		std::string name() const;
		std::string classType() const;
	};
}