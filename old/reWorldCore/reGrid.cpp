#include "StdAfx.h"
#include "reGrid.h"
#include "reRandom.h"
#include <complex>
void reWorldCore::reGrid::generateSeed()
{
	reRandom rX(*reinterpret_cast<unsigned int*>(&this->mX));
	reRandom rY(*reinterpret_cast<unsigned int*>(&this->mY));
	int absX = std::abs(this->mX);
	int absY = std::abs(this->mY);
	for(int i=0; i<absX; ++i)
	{
		rY.getNumber();
	}
	for(int j=0; j<absY; ++j)
	{
		rX.getNumber();
	}
	this->mSeed = rX.getNumber() + rY.getNumber();
}

reWorldCore::reGrid::reGrid( int x, int y ) : mX(x), mY(y)
{
	generateSeed();
}

reWorldCore::reGrid::~reGrid( void )
{

}