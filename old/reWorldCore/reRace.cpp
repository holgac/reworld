#include "StdAfx.h"
#include "reRace.h"
#include "reAbilities.h"
#include "reVision.h"
using namespace reWorldCore;

bool reWorldCore::reRace::reRaceFeat::validForLevel( int level )
{
	if(mAt == level)
	{
		return true;
	}
	if(level > mAt && mRecurringAmount != 0)
	{
		int rem = level-mAt;
		rem = rem % mRecurringAmount;
		if(rem == 0)
		{
			return true;
		}
	}
	return false;
}

int reWorldCore::reRace::reRaceFeat::amount() const
{
	return mAmount;
}

void reWorldCore::reRace::reRaceFeat::parseXML( const reXMLNode* node )
{
	mAt = reStringTools::getValue<int>(node->attribute("at")->value());
	mAmount = reStringTools::getValue<int>(node->attribute("amount")->value());
	const reXMLAttribute* attrib = node->attribute("recurringAmount");
	if(attrib != nullptr)
	{
		mRecurringAmount = reStringTools::getValue<int>(attrib->value());
	}
	else
	{
		mRecurringAmount = 0;
	}
}

void reWorldCore::reRace::reRaceLanguage::parseXML( const reXMLNode* node )
{
	const reXMLAttribute* attrib = node->attribute("value");
	if(attrib != nullptr)
	{
		mLanguage = (reLanguage*)reDataManager<reLanguage>::getInstance().data(attrib->value());
	}
	else
	{
		mLanguage = nullptr;
	}
	mIsBonusLanguage = false;
	attrib = node->attribute("type");
	if(attrib != nullptr)
	{
		if(attrib->value() == "bonus")
		{
			mIsBonusLanguage = true;
		}
	}
	attrib = node->attribute("exceptClass");
	if(attrib != nullptr)
	{
		mExceptClass = attrib->value();
	}
}

bool reWorldCore::reRace::reRaceLanguage::isBonusLanguage() const
{
	return mIsBonusLanguage;
}

bool reWorldCore::reRace::reRaceLanguage::appliesFor( const reLanguage* lang )
{
	if(mLanguage == lang)
	{
		return true;
	}
	if(mLanguage == nullptr)
	{
		return lang->classType() != mExceptClass;
	}
	return false;
}

int reWorldCore::reRace::reRaceAbility::abilityId() const
{
	return mAbilityId;
}

int reWorldCore::reRace::reRaceAbility::value() const
{
	return mValue;
}

void reWorldCore::reRace::reRaceAbility::parseXML( const reXMLNode* node )
{
	std::string str = node->attribute("name")->value();
	mAbilityId = reAbility::getIndexFromShortName(str);
	mValue = reStringTools::getValue<int>(node->attribute("value")->value());
}

void reWorldCore::reRace::reRaceLandSpeed::parseXML( const reXMLNode* node )
{
	mValue = reStringTools::getValue<float>(node->attribute("value")->value());
	const reXMLAttribute* attrib = node->attribute("ignoreArmor");
	if(attrib != nullptr)
	{
		mIgnoresArmor = reStringTools::getValue<bool>(attrib->value());
	}
	else
	{
		mIgnoresArmor = false;
	}
	attrib = node->attribute("ignoreLoad");
	if(attrib != nullptr)
	{
		mIgnoresLoad = reStringTools::getValue<bool>(attrib->value());
	}
	else
	{
		mIgnoresLoad = false;
	}
}

reWorldCore::reRace::reRaceVision::reRaceVision() : mVision(reVision::Regular), mDistance(-1)
{

}

void reWorldCore::reRace::reRaceVision::parseXML( const reXMLNode* node )
{
	const reXMLAttribute* attrib = node->attribute("value");
	if(attrib != nullptr)
	{
		mVision = reVision::getIndexFromName(attrib->value());
	}
	attrib = node->attribute("distance");
	if(attrib != nullptr)
	{
		mDistance = reStringTools::getValue<int>(attrib->value());
	}
}