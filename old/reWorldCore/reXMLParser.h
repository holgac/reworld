#pragma once
#include <string>
#include <list>
#include "../external/rapidxml-1.13/rapidxml.hpp"

namespace reWorldCore
{
	class reXMLAttribute
	{
	private:
		std::string mName;
		std::string mValue;
	public:
		reXMLAttribute(std::string name, std::string value);
		const std::string& name() const;
		void setName(std::string val);
		const std::string& value() const;
		void setValue(std::string val);
	};
	class reXMLNode
	{
	private:
		std::string mName;
		std::list<reXMLAttribute*> mAttributes;
		std::list<reXMLNode*> mNodes;
	public:
		reXMLNode(std::string name);
		~reXMLNode();
		const std::string& name() const;
		void setName(std::string val);
		// the reXMLAttribute instance is owned by reXMLNode
		// after the call.
		void addAttribute(reXMLAttribute* attr);
		std::list<reXMLAttribute*>::const_iterator attributesBegin() const;
		std::list<reXMLAttribute*>::const_iterator attributesEnd() const;
		const reXMLAttribute* attribute(std::string name) const;
		std::list<const reXMLAttribute*> attributes(std::string name) const;
		// the reXMLNode instance is owned by reXMLNode
		// after the call.
		void addNode(reXMLNode* node);
		std::list<reXMLNode*>::const_iterator nodesBegin() const;
		std::list<reXMLNode*>::const_iterator nodesEnd() const;
		const reXMLNode* node(std::string name) const;
		std::list<const reXMLNode*> nodes(std::string name) const;
	};
	class reXMLParser
	{
		static void parseSubnode(rapidxml::xml_node<>* node, reXMLNode* parentNode);
	public:
		static reXMLNode* parseFile(std::string filename);
		template <typename T>
		static void parseFieldFromString(const reXMLNode* node, T& field, const T& defaultValue, const::std::string& nodeName)
		{
			const reXMLNode* subnode = node->node(nodeName);
			if(subnode != nullptr)
			{
				field = reStringTools::getValue<T>(subnode->attribute("value")->value());
			}
			else
			{
				field = defaultValue;
			}
		}

		template <typename T>
		static void parseField(const reXMLNode* node, T& field, const std::string& nodeName)
		{
			const reXMLNode* subnode = node->node(nodeName);
			if(subnode != nullptr)
			{
				field.parseXML(subnode);
			}
		}

		template <typename T>
		static void parseField(const reXMLNode* node, T& field, const std::string& nodeName, const std::string& arg2)
		{
			const reXMLNode* subnode = node->node(nodeName);
			if(subnode != nullptr)
			{
				field.parseXML(subnode, arg2);
			}
		}

	};

}