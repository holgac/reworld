import json, os, random


class reGridType:
	def __init__(self, grid_data):
		self.data = grid_data
		self.improvement_prob_count = 0
		if 'improvements' in self.data:
			for improvements in self.data['improvements']:
				self.improvement_prob_count += improvements['probability']
	def name(self):
		return self.data['name']
	def probability(self):
		return self.data['probability']
	def is_passable(self):
		return self.data['passable'] == 'True'
	def actions(self):
		return self.data['actions']
	def __str__(self):
		info = ''
		if(not self.is_passable()):
			info = ' (unpassable)'
		return self.name() + info

class reGridAction:
	def __init__(self, action_data):
		self.data = action_data
	def conditions_pass(self, game):
		conds = self.data['condition']
		for cond in conds:
			if cond == 'self':
				f = {}
				exec('def f(arg1):\n\treturn ' + conds[cond]) in f
				if f['f'](game.player) == False:
					return False
			if cond == 'game':
				f = {}
				exec('def f(arg1):\n\treturn ' + conds[cond]) in f
				if f['f'](game) == False:
					return False
		return True
	def commit(self, game):
		actions = self.data['action']
		for action in actions:
			if action == 'self':
				f = {}
				exec('def f(arg1):\n\t' + actions[action]) in f
				f['f'](game.player)
			if action == 'game':
				f = {}
				exec('def f(arg1):\n\t' + actions[action]) in f
				f['f'](game)
	def __str__(self):
		return self.data['string']

class reRace:
	def __init__(self, race_data):
		self.data = race_data
	def is_selectable(self):
		return self.data['selectable'] == 'True'
	def name(self):
		return self.data['name']
	def possession_name(self):
		if 'possessionName' in self.data:
			return self.data['possessionName']
		return self.name()
	def __str__(self):
		return self.name()

class reClass:
	def __init__(self, class_data):
		self.data = class_data
	def name(self):
		return self.data['name']
	def hit_die(self):
		return self.data['hitDie']
	def is_selectable(self):
		return self.data['selectable'] == "True"
	def __str__(self):
		return self.name()

class reCharacter:
	def __init__(self):
		self.name = None
		self.level = 1
		self.hp = 0
		self.max_hp = 0
		self.abilities={ 'STR':0, 'DEX':0, 'CON':0, 'INT':0, 'WIS':0, 'CHA':0}
		self.race = None
		self.class_type = None
	def __str__(self):
		beg_str=''
		base_str = ''
		if self.name != None:
			base_str += '{0}{1} ({2} {3})\n'.format(beg_str, self.name, self.race.possession_name(), self.class_type)
		else:
			base_str += beg_str + 'A {0} {1}\n'.format(self.race.possession_name(), self.class_type)
		base_str += '{0}\tHP: {1}/{2}\n'.format(beg_str, self.hp, self.max_hp)
		base_str += beg_str + '\tAbilities:\n'
		base_str += self.print_abilities(beg_str + '\t\t')
		return base_str
	def is_alive(self):
		return self.hp > 0


	def print_abilities(self, beg_str=''):
		base_str = ''
		for ab in self.abilities.items():
			base_str += '{0}{1}: {2}\n'.format(beg_str, ab[0], ab[1])
		return base_str

	def randomize_abilities(self, world):
		for ab in self.abilities:
			self.abilities[ab] = world.throw_dice('3d6')
		self.max_hp = 0
		self.hp = 0
		self.calculate_max_hp(world)
	
	def calculate_max_hp(self, world):
		old_max_hp = self.max_hp
		self.max_hp += max(world.throw_dice(self.class_type.hit_die())+self.get_modifier('CON'), 1)
		diff = self.max_hp - old_max_hp
		self.hp += diff

	def get_modifier(self, ability):
		ab = self.abilities[ability]
		return (ab-10)/2

class reWeapon:
	def __init__(self):
		self.name = ''
