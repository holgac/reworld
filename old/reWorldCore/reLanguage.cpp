#include "StdAfx.h"
#include "reLanguage.h"
#include "reConfiguration.h"
#include "reXMLParser.h"

using namespace reWorldCore;

void reWorldCore::reLanguage::parseXML( const reXMLNode* node )
{
	const reXMLAttribute* attrib = node->attribute("name");
	mName = attrib->value();
	attrib = node->attribute("class");
	mClass = attrib->value();
}

reWorldCore::reLanguage::reLanguage( void )
{

}

reWorldCore::reLanguage::~reLanguage( void )
{

}

std::string reWorldCore::reLanguage::name() const
{
	return mName;
}

std::string reWorldCore::reLanguage::classType() const
{
	return mClass;
}