#pragma once
namespace reWorldCore
{
	/*
	 * This class currently uses Mersenne twister algorithm
	 * http://en.wikipedia.org/wiki/Mersenne_twister
	*/
	class reRandom
	{
	private:
		unsigned int index;
		unsigned int mersenneTwister[624];
		void generateNumbers();
	public:
		/*
		 * creates a new reRandom instance
		 * that generates pseudo random
		 * numbers based on seed
		 * call initialise before calling getNumber!
		 */
		reRandom();
		reRandom(unsigned int seed);

		void initialise(unsigned int seed);
		/*
		 * returns the next pseudo random number
		 */
		unsigned int getNumber();
	};
}