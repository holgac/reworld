import json, os, random
REWORLD_ROOT_PATH = os.getcwd()
from recore import reRace, reClass, reCharacter, reWeapon, reGridType, reGridAction


class reGrid:
	def __init__(self, world, x, y):
		self.world = world
		self.x = x
		self.y = y
		# TODO: implement a better algorithm for pseudo random numbers
		seed_x = ((x+world.grid_seed[3]) << ((self.y-world.grid_seed[0])%29)) & world.grid_seed[2]
		seed_y = ((y-world.grid_seed[4]) << ((self.x+world.grid_seed[1])%29)) & world.grid_seed[3]
		seed = seed_x | seed_y
		world.rand.push(seed)
		val = random.random()
		self.grid_type = world.get_grid(val)
		#print('seedx: {0} seedy: {1}, seed: {2}, randVal = {3}'.format(seed_x, seed_y, seed, val))
		world.rand.pop()

	def __str__(self):
		return '{2} at ({0},{1})'.format(self.x, self.y, self.grid_type)

# TODO: make all random calls from here.
class reRandom:
	def __init__(self):
		self.rand_states = []

	def push(self, new_value):
		self.rand_states.append(random.getstate())
		random.seed(new_value)
	def pop(self):
		st = self.rand_states[len(self.rand_states)-1]
		self.rand_states = self.rand_states[:len(self.rand_states)-1]
		random.setstate(st)

class reWorld:
	def __init__(self):
		self.world_data = {}
		self.races = []
		self.race_map = {}
		self.classes = []
		self.class_map = {}
		self.grid_types = []
		self.grid_type_map = {}
		self.grid_actions = []
		self.grid_action_map = {}
		self.grid_probability_count = 0
		self.rand = reRandom()

		self.grid_seed = []
		self.grid_seed.append(0x2aae5e5d)
		self.grid_seed.append(0x5a801475)
		self.grid_seed.append(0x3a532db0)
		self.grid_seed.append(0x393af629)
		self.grid_seed.append(0x03f3d918)
		self.grid_seed.append(0xd21fd2ea)

		self.load_data()
		self.parse_data()

	def load_datafile(self, lang_path):
		f = open(lang_path, 'r')
		data = json.loads(f.read())
		self.world_data.update(data)

	def get_language(self, lang_name):
		ret_val = dict(self.world_data['languages']['*'])
		lang = self.world_data['languages'][lang_name]
		ret_val.update(lang)
		return ret_val

	def load_data(self):
		self.load_datafile('bin/languages.json')
		self.load_datafile('bin/races.json')
		self.load_datafile('bin/classes.json')
		self.load_datafile('bin/grids.json')
		self.load_datafile('bin/grid_improvements.json')
		self.load_datafile('bin/grid_actions.json')

	def get_grid(self, probability):
		new_prob = probability * self.grid_probability_count

		for grid_type in self.grid_types:
			if new_prob < grid_type.probability():
				return grid_type
			new_prob -= grid_type.probability()

	def get_grid_action(self, action):
		return self.grid_action_map[action]

	def parse_data(self):
		for race_data in self.world_data['races']:
			if race_data == '*':
				continue

			cur_race_data = self.world_data['races'][race_data]
			default_race_data = dict(self.world_data['races']['*'])
			default_race_data.update(cur_race_data)

			race = reRace(default_race_data)
			self.races.append(race)
			self.race_map[race_data] = self.races[len(self.races)-1]

		for class_data in self.world_data['classes']:
			if class_data == '*':
				continue

			cur_class_data = self.world_data['classes'][class_data]
			default_class_data = dict(self.world_data['classes']['*'])
			default_class_data.update(cur_class_data)

			class_instance = reClass(default_class_data)
			self.classes.append(class_instance)
			self.class_map[class_data] = self.classes[len(self.classes)-1]

		for grid_data in self.world_data['grids']:
			if grid_data == '*':
				continue

			cur_grid_data = self.world_data['grids'][grid_data]
			default_grid_data = dict(self.world_data['grids']['*'])
			default_grid_data.update(cur_grid_data)
			if 'actions' in cur_grid_data:
				default_grid_data['actions'] = list(set(default_grid_data['actions'] + cur_grid_data['actions']))

			grid = reGridType(default_grid_data)
			self.grid_probability_count += grid.probability()
			self.grid_types.append(grid)
			self.grid_type_map[grid_data] = self.grid_types[len(self.grid_types)-1]

		for action_data in self.world_data['grid_actions']:
			if action_data == '*':
				continue

			cur_action_data = self.world_data['grid_actions'][action_data]
			default_action_data = dict(self.world_data['grid_actions']['*'])
			default_action_data.update(cur_action_data)

			action = reGridAction(default_action_data)
			self.grid_actions.append(action)
			self.grid_action_map[action_data] = self.grid_actions[len(self.grid_actions)-1]

	def throw_dice(self, dice):
		d_index = dice.find('d')
		count = 1
		if d_index > 0:
			count = int(dice[:d_index])
		dice = int(dice[d_index+1:])
		total = 0
		for i in range(count):
			total += random.randint(1,dice)
		return total

class reGame:
	def __init__(self):
		self.world = reWorld()
		self.player = reCharacter()
		self.current_x = 0
		self.current_y = 0
		self.current_grid = reGrid(self.world, self.current_x, self.current_y)

	def move_player(self, x, y):
		self.current_x = x
		self.current_y = y
		self.current_grid = reGrid(self.world, self.current_x, self.current_y)

	def take_a_look_around(self):
		w = reGrid(self.world, self.current_x-1, self.current_y)
		e = reGrid(self.world, self.current_x+1, self.current_y)
		n = reGrid(self.world, self.current_x, self.current_y+1)
		s = reGrid(self.world, self.current_x, self.current_y-1)
		print('North: {0}'.format(n))
		print('East: {0}'.format(e))
		print('South: {0}'.format(s))
		print('West: {0}'.format(w))

	def grid_is_passable(self, x, y):
		grid = reGrid(self.world, x, y)
		return grid.grid_type.is_passable()

	def create_player(self):
		self.player.race = self.world.race_map['Dwarf']
		self.player.class_type = self.world.class_map['Fighter']
		self.player.name = 'ravenoth'
		self.player.randomize_abilities(self.world)
		print(self.player)

	def choose_race(self):
		print("Choose Race:")
		counter = 0
		races = []
		for race in self.world.races:
			if race.is_selectable():
				print(str(counter) + ": " + race.name())
				counter += 1
				races.append(race)
		# TODO: check for success in parsing
		# TODO: check for success in indexing
		choice = int(raw_input())
		self.player.race = races[choice]
	def choose_class(self):
		print("Choose Class:")
		counter = 0
		classes = []
		for class_instance in self.world.classes:
			if class_instance.is_selectable():
				print(str(counter) + ": " + class_instance.name())
				counter += 1
				classes.append(class_instance)
		# TODO: check for success in parsing
		# TODO: check for success in indexing
		choice = int(raw_input())
		self.player.class_type = classes[choice]

	def choose_name(self):
		print("Choose Name:");
		self.player.name=raw_input()

	def choose_abilities(self):
		choice = 'r'
		while choice == 'r':
			self.player.randomize_abilities(self.world)
			print(self.player)
			print('enter r to retry')
			choice = raw_input()

	def game_loop(self):
		print(self.current_grid)
		print("What do you do?")
		available_actions = []
		counter = 0
		grid_actions = self.current_grid.grid_type.actions()
		for grid_action in grid_actions:
			action = self.world.get_grid_action(grid_action)
			if action.conditions_pass(self):
				print('{0}: {1}'.format(counter, action))
				available_actions.append(action)
				counter += 1
		choice = int(raw_input())
		action = available_actions[choice]
		action.commit(self)
		# TODO: print actions, implement action class that gridType uses.
		return True

	def start(self):
#		self.choose_race()
#		self.choose_class()
#		self.choose_name()
#		self.choose_abilities()
		self.create_player()
		val = True
		while val:
			val = self.game_loop()

g = reGame()
g.start()
