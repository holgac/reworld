#pragma once
#include <string>

namespace reWorldCore
{
	class reBuilding
	{
	public:
		reBuilding(void)
		{

		}
		virtual ~reBuilding(void)
		{

		}

		virtual bool shownOnDisplay() const=0;
		virtual std::string displayName() const=0;
	};

}