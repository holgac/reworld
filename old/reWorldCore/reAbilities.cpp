#include "StdAfx.h"
#include "reAbilities.h"

reWorldCore::reAbilities::reAbilities( void )
{

}

reWorldCore::reAbilities::~reAbilities( void )
{

}

void reWorldCore::reAbilities::printAbilities( std::ostream ostr ) const
{
	for(int i=0; i<reAbility::Count; ++i)
	{
		ostr << reAbility::AbilityNamesShort[i] << " " << this->mAbilities[i] << std::endl;
	}
}

int reWorldCore::reAbilities::getAbility( int index ) const
{
	return mAbilities[index];
}

int reWorldCore::reAbilities::getModifier( int index ) const
{
	int score = getAbility(index);
	if(index < 10)
	{
		score--;
	}
	return (score - 10) / 2;
}