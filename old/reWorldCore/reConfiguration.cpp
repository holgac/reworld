#include "StdAfx.h"
#include "reConfiguration.h"
#include <fstream>
#include <vector>
#include <streambuf>
#include "reXMLParser.h"

void reWorldCore::reConfiguration::initialise( std::string configurationFile /*= "configuration.xml"*/ )
{
	mConfigurationXML = reWorldCore::reXMLParser::parseFile(configurationFile);
}

std::string reWorldCore::reConfiguration::config( std::string configName ) const
{
	const reWorldCore::reXMLNode* node = mConfigurationXML->node(configName);
	if(node == nullptr)
	{
		return std::string("");
	}
	else
	{
		const reWorldCore::reXMLAttribute* attrib = node->attribute("value");
		if(attrib == nullptr)
		{
			return std::string("");
		}
		return attrib->value();
	}
}