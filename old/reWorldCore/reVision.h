#pragma once
#include <sstream>
namespace reWorldCore
{
	namespace reVision
	{
		enum Vision
		{
			Regular,
			LowLight,
			Dark,
			Count,
		};
		const char* VisionNames[]=
		{
			"Regular",
			"LowLight",
			"Dark",
		};
		int getIndexFromName(std::string name)
		{
			for(int i=0; i<Count; ++i)
			{
				if(name == VisionNames[i])
				{
					return i;
				}
			}
			return (int)Count;
		}
	}
}