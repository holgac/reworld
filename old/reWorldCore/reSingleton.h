#pragma once
namespace reWorldCore
{

	template <typename T>
	class reSingleton
	{
	private:
		static T* mPtr;
	protected:
		reSingleton(void)
		{
			mPtr = nullptr;
		}
		virtual ~reSingleton(void)
		{
		}

	public:
		static void createInstance()
		{
			mPtr = new T;
		}
		static void deleteInstance()
		{
			delete mPtr;
		}
		static T* getInstancePtr()
		{
			return mPtr;
		}
		static T& getInstance()
		{
			return *mPtr;
		}
	};
	template <typename T>
	T* reSingleton<T>::mPtr = nullptr;
}