#pragma once
#include <vector>
#include "reSingleton.h"
#include "reXMLParser.h"
#include "reStringTools.h"
#include "reLanguage.h"
#include "reDataManager.h"

namespace reWorldCore
{
	class reRaceManager;
	class reRace
	{
	public:
		class reRaceFeat
		{
		friend class reRace;
		private:
			int mAt;
			int mAmount;
			int mRecurringAmount;
			void parseXML(const reXMLNode* node);
		public:
			bool validForLevel(int level);
			int amount() const;
		};
		class reRaceLanguage
		{
		private:
			reLanguage* mLanguage;
			std::string mExceptClass;
			bool mIsBonusLanguage;
			void parseXML(const reXMLNode* node);
		public:
			reLanguage* language() const { return mLanguage; }
			bool appliesFor(const reLanguage* lang);
			bool isBonusLanguage() const;
		};
		class reRaceAbility
		{
		private:
			int mAbilityId;
			int mValue;
			void parseXML(const reXMLNode* node);
		public:
			int abilityId() const;
			int value() const;
		};
		template <typename T>
		class reRaceField
		{
			friend class reRace;
		private:
			std::vector<T> mData;
			void parseXML(const reXMLNode* node, std::string fieldName)
			{
				for(auto it=node->nodesBegin(), end=node->nodesEnd(); it!=end; ++it)
				{
					if((*it)->name() == fieldName)
					{
						T field;
						field.parseXML(*it);
						mData.push_back(field);
					}
				}
			}
		public:
			typename std::vector<T>::const_iterator dataBegin() const
			{
				return mData.cbegin();
			}
			typename std::vector<T>::const_iterator dataEnd() const
			{
				return mData.cend();
			}
		};
		class reRaceLandSpeed
		{
		friend class reRace;
		private:
			void parseXML(const reXMLNode* node);
			int mValue;
			bool mIgnoresArmor;
			bool mIgnoresLoad;
		public:
			int value() const { return mValue; }
			bool ignoresArmor() const { return mIgnoresArmor; }
			bool ignoresLoad() const { return mIgnoresLoad; }
		};
		class reRaceVision
		{
		friend class reRace;
		private:
			int mVision;
			int mDistance;
			reRaceVision();
			void parseXML(const reXMLNode* node);
		public:
		};
	private:
		friend class reRaceManager;
		std::string mName;
		std::string mPluralName;
		reRaceField<reRaceFeat> mRaceFeats;
		reRaceField<reRaceLanguage> mRaceLanguages;
		reRaceField<reRaceAbility> mRaceAbilities;
		reRaceField<reRaceAbility> mRaceMinAbilities;
		reRaceLandSpeed mBaseLandSpeed;
		bool mSelectable;
		reRaceVision mVision;

		void parseXML(const reXMLNode* node)
		{
			std::string defaultName("invalid");
			reXMLParser::parseFieldFromString(node, mName, defaultName, "name");
			reXMLParser::parseFieldFromString(node, mPluralName, defaultName, "pluralName");
			reXMLParser::parseFieldFromString(node, mSelectable, false, "selectable");
			reXMLParser::parseField(node, mBaseLandSpeed, "baseLandSpeed");
			reXMLParser::parseField(node, mRaceAbilities, "abilities", "ability");
			reXMLParser::parseField(node, mRaceMinAbilities, "minAbilities", "minAbility");
			reXMLParser::parseField(node, mRaceFeats, "feats", "feat");
			reXMLParser::parseField(node, mRaceLanguages, "languages", "language");
			reXMLParser::parseField(node, mVision, "vision");
			// TODO_FEATURE: parse weapon proficencies after implementing weapon classes
			// TODO_FEATURE: parse weapon familiarities after implementing weapon classes
			// TODO_FEATURE: parse blood type after implementing race classes (ie goblinoid, humanoid etc.)
			// TODO_FEATURE: parse attack roll bonuses after implementing race classes (ie goblinoid, humanoid etc.)
			// TODO_FEATURE: parse armor class bonuses after implementing race classes (ie goblinoid, humanoid etc.)
			// TODO_FEATURE: parse skills after implementing skill classes
			// TODO_FEATURE: parse favored class after implementing class classes
			// TODO_FEATURE: parse spell like effects after implementing spell and spell like classes
			// TODO_FEATURE: parse saving throws after ???
			// TODO_FEATURE: parse immunities after ???

		}
	public:
		bool selectable() const { return mSelectable; }
		reRace(void)
		{
			
		}
		~reRace(void)
		{

		}
	};
}