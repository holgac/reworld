#pragma once
#include <string>
namespace reWorldCore
{
	namespace reAlignments
	{
		enum Goodness
		{
			GGood,
			GNeutral,
			GEvil,
			GCount,
		};
		const char* GoodnessNames[]=
		{
			"Good",
			"Neutral",
			"Evil",
		};
		enum Lawfulness
		{
			LLawful,
			LNeutral,
			LChaotic,
			LCount,
		};
		const char* LawfulnessNames[]=
		{
			"Lawful",
			"Neutral",
			"Chaotic",
		};

		const char* AlignmentNames[3][3]=
		{
			{
				"Lawful Good",
				"Neutral Good",
				"Chaotic Good",
			},
			{
				"Lawful Neutral",
				"Neutral",
				"Chaotic Neutral",
			},
			{
				"Lawful Evil",
				"Neutral Evil",
				"Chaotic Evil",
			},
		};
		const char* AlignmentNamesShort[3][3]=
		{
			{
				"LG",
				"NG",
				"CG",
			},
			{
				"LN",
				"N",
				"CN",
			},
			{
				"LE",
				"NE",
				"CE",
			},
		};
		void getAlignmentFromName(const std::string& name, Goodness& goodness, Lawfulness& lawfulness)
		{
			for(int i=0; i<GCount; ++i)
			{
				for(int j=0; j<LCount; ++j)
				{
					if(name == AlignmentNames[i][j])
					{
						goodness = (Goodness)i;
						lawfulness = (Lawfulness)j;
						return;
					}
				}
			}
		}
		void getAlignmentFromShortName(const std::string& name, Goodness& goodness, Lawfulness& lawfulness)
		{
			for(int i=0; i<GCount; ++i)
			{
				for(int j=0; j<LCount; ++j)
				{
					if(name == AlignmentNamesShort[i][j])
					{
						goodness = (Goodness)i;
						lawfulness = (Lawfulness)j;
						return;
					}
				}
			}
		}

		const char* AlignmentTitles[3][3]=
		{
			{
				"Crusader",
				"Benefactor",
				"Rebel",
			},
			{
				"Judge",
				"Undecided",
				"Free Spirit",
			},
			{
				"Dominator",
				"Malefactor",
				"Destroyer",
			},
		};
	};
	class reAlignment
	{
		int mGoodness;
		int mLawfulness;
	public:
		reAlignment(void)
		{

		}
		~reAlignment(void)
		{

		}
	};

}