#include "StdAfx.h"
#include "reRandom.h"

void reWorldCore::reRandom::initialise( unsigned int seed )
{
	this->mersenneTwister[0] = seed;
	for(int i=1; i<624; ++i)
	{
		this->mersenneTwister[i] = 1812433253 * (this->mersenneTwister[i-1] ^ ((this->mersenneTwister[i-1]<<30) + i));
	}
}

reWorldCore::reRandom::reRandom()
{

}

reWorldCore::reRandom::reRandom( unsigned int seed )
{
	initialise(seed);
}

void reWorldCore::reRandom::generateNumbers()
{
	for(int i=0; i<624; ++i)
	{
		int y = this->mersenneTwister[i] & 0x80000000;
		y += this->mersenneTwister[i+1 % 624];
		this->mersenneTwister[i] = this->mersenneTwister[(i+1) % 624] & 0x7fffffff;
		this->mersenneTwister[i] = this->mersenneTwister[(i+397) % 624] ^ (y<<1);
		if(y%2)
		{
			this->mersenneTwister[i] = this->mersenneTwister[i] ^ 0x9908b0df;
		}
	}
}

unsigned int reWorldCore::reRandom::getNumber()
{
	if(this->index == 0)
	{
		generateNumbers();
	}
	int y = this->mersenneTwister[index];
	y ^= (y>>11);
	y ^= ((y<<7) & 0x9d2c5680);
	y ^= ((y<<15) & 0xefc60000);
	y ^= (y>>18);
	index = (index+1) % 624;
	return y;
}