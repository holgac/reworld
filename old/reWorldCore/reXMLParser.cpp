#include "StdAfx.h"
#include "reXMLParser.h"
#include <fstream>
#include <streambuf>
#include <vector>

reWorldCore::reXMLAttribute::reXMLAttribute( std::string name, std::string value ) : mName(name), mValue(value)
{

}

const std::string& reWorldCore::reXMLAttribute::name() const
{
	return mName;
}

void reWorldCore::reXMLAttribute::setName( std::string val )
{
	mName = val;
}

const std::string& reWorldCore::reXMLAttribute::value() const
{
	return mValue;
}

void reWorldCore::reXMLAttribute::setValue( std::string val )
{
	mValue = val;
}

reWorldCore::reXMLNode::reXMLNode( std::string name ) : mName(name)
{

}

reWorldCore::reXMLNode::~reXMLNode()
{
	for(auto it = mAttributes.begin(), end = mAttributes.end(); it!=end; ++it)
	{
		delete (*it);
	}
	for(auto it = mNodes.begin(), end = mNodes.end(); it!=end; ++it)
	{
		delete (*it);
	}
}

const std::string& reWorldCore::reXMLNode::name() const
{
	return mName;
}

void reWorldCore::reXMLNode::setName( std::string val )
{
	mName = val;
}

void reWorldCore::reXMLNode::addAttribute( reWorldCore::reXMLAttribute* attr )
{
	mAttributes.push_back(attr);
}

std::list<reWorldCore::reXMLAttribute*>::const_iterator reWorldCore::reXMLNode::attributesBegin() const
{
	return mAttributes.cbegin();
}

std::list<reWorldCore::reXMLAttribute*>::const_iterator reWorldCore::reXMLNode::attributesEnd() const
{
	return mAttributes.cend();
}

const reWorldCore::reXMLAttribute* reWorldCore::reXMLNode::attribute( std::string name ) const
{
	for(auto it = mAttributes.begin(), end = mAttributes.end(); it!=end; ++it)
	{
		if((*it)->name() == name)
		{
			return *it;
		}
	}
	return nullptr;
}

std::list<const reWorldCore::reXMLAttribute*> reWorldCore::reXMLNode::attributes( std::string name ) const
{
	std::list<const reXMLAttribute*> attrList;
	for(auto it = mAttributes.begin(), end = mAttributes.end(); it!=end; ++it)
	{
		if((*it)->name() == name)
		{
			attrList.push_back(*it);
		}
	}
	return attrList;
}

void reWorldCore::reXMLNode::addNode( reWorldCore::reXMLNode* node )
{
	mNodes.push_back(node);
}

std::list<reWorldCore::reXMLNode*>::const_iterator reWorldCore::reXMLNode::nodesBegin() const
{
	return mNodes.cbegin();
}

std::list<reWorldCore::reXMLNode*>::const_iterator reWorldCore::reXMLNode::nodesEnd() const
{
	return mNodes.cend();
}

const reWorldCore::reXMLNode* reWorldCore::reXMLNode::node( std::string name ) const
{
	for(auto it = mNodes.begin(), end = mNodes.end(); it!=end; ++it)
	{
		if((*it)->name() == name)
		{
			return *it;
		}
	}
	return nullptr;
}

std::list<const reWorldCore::reXMLNode*> reWorldCore::reXMLNode::nodes( std::string name ) const
{
	std::list<const reWorldCore::reXMLNode*> nodeList;
	for(auto it = mNodes.begin(), end = mNodes.end(); it!=end; ++it)
	{
		if((*it)->name() == name)
		{
			nodeList.push_back(*it);
		}
	}
	return nodeList;
}

reWorldCore::reXMLNode* reWorldCore::reXMLParser::parseFile( std::string filename )
{
	reWorldCore::reXMLNode* rootNode = new reWorldCore::reXMLNode("ROOT");
	std::ifstream fin(filename.c_str());
	std::vector<char> buffer((std::istreambuf_iterator<char>(fin)), std::istreambuf_iterator<char>());
	buffer.push_back('\0');
	rapidxml::xml_document<> doc;
	doc.parse<0>(&buffer[0]);
	for(rapidxml::xml_node<>* node = doc.first_node(); node!=nullptr; node=node->next_sibling())
	{
		parseSubnode(node, rootNode);
	}
	return rootNode;
}

void reWorldCore::reXMLParser::parseSubnode( rapidxml::xml_node<>* node, reWorldCore::reXMLNode* parentNode )
{
	reXMLNode* subnode = new reXMLNode(node->name());
	for(rapidxml::xml_attribute<>* attr = node->first_attribute(); attr!=nullptr; attr=attr->next_attribute())
	{
		subnode->addAttribute(new reWorldCore::reXMLAttribute(attr->name(), attr->value()));
	}
	for(rapidxml::xml_node<>* childNode=node->first_node(); childNode!=nullptr; childNode=childNode->next_sibling())
	{
		parseSubnode(childNode, subnode);
		parentNode->addNode(subnode);
	}
}
