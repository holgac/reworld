#pragma once
#include <string>
#include <sstream>
#include <vector>

namespace reWorldCore
{

	class reStringTools
	{
	public:
		// removes tab, endline and space from the beginning and end.
		static void trimString(std::string& str)
		{
			int beg = str.find_first_not_of(" \t\n");
			int end = str.find_last_not_of(" \t\n");
			str = str.substr(beg,end-beg+1);
		}

		// parses the string to the specified type
		template <typename T>
		static T getValue(const std::string& str)
		{
			T x;
			std::stringstream stream(str);
			stream >> x;
			return x;
		}

		template <>
		static bool getValue<bool>(const std::string& str)
		{
			if(str.empty())
				return false;
			switch(str[0])
			{
			case '0':
			case 'F':
			case 'f':
				return false;
			case 'T':
			case 't':
			case '1':
				return true;
			default:
				return false;
			}
		}

		// parses the string to the vector of specified type.
		template <class T>
		static std::vector<T> makeVector(const std::string& _str, const char* blanks=" ,")
		{
			std::vector<T> ret;
			const char* str = _str.c_str();
			const char* oldPos=str;
			const char* p;
			for(; *str!=NULL; ++str)
			{
				if(p = strchr(blanks, *str))
				{
					std::string temp(oldPos, str-oldPos);
					ret.push_back(GetValue<T>(temp));
					for(; *str!=NULL; ++str)
					{
						if(!strchr(blanks, *str))
							break;
					}
					oldPos = str;
					if(*str == NULL)
						break;
				}
			}
			if(oldPos != str)
			{
				std::string temp(oldPos, str-oldPos);
				ret.push_back(GetValue<T>(temp));
			}
			return ret;
		}
		// corrects \\n to \n, \\t to \t, \\b to \b
		static std::string formatText(const std::string& str)
		{
			std::string ret;
			unsigned int x=str.find_first_of('\\'), size=str.size();
			unsigned int oldPos=0;
			while(x<size && x!=std::string::npos)
			{
				ret.append(str, oldPos, x-oldPos);
				oldPos = x+2;
				switch(str[x+1])
				{
				case 'n':
					ret += '\n';
					break;
				case 't':
					ret += '\t';
					break;
				case 'b':
					ret += '\b';
					break;
				}
				x = str.find_first_of('\\', oldPos);
			}
			if( oldPos < size )
			{
				ret.append(str, oldPos, size-oldPos);
			}
			return ret;
		}
		// Parses the value(s) from the string
		template <typename T1>
		static void fromString(std::string s, T1& _1)
		{
			std::stringstream stream(s);
			stream >> _1;
		}
		// Parses the value(s) from the string
		template <typename T1, typename T2>
		static void fromString(std::string s, T1& _1, T2& _2)
		{
			std::stringstream stream(s);
			stream >> _1 >> _2;
		}
		// Parses the value(s) from the string
		template <typename T1, typename T2, typename T3>
		static void fromString(std::string s, T1& _1, T2& _2, T3& _3)
		{
			std::stringstream stream(s);
			stream >> _1 >> _2 >> _3;
		}
		// Parses the value(s) from the string
		template <typename T1, typename T2, typename T3, typename T4>
		static void fromString(std::string s, T1& _1, T2& _2, T3& _3, T4& _4)
		{
			std::stringstream stream(s);
			stream >> _1 >> _2 >> _3 >> _4;
		}
		// Parses the value(s) from the string
		template <typename T1, typename T2, typename T3, typename T4, typename T5>
		static void fromString(std::string s, T1& _1, T2& _2, T3& _3, T4& _4, T5& _5)
		{
			std::stringstream stream(s);
			stream >> _1 >> _2 >> _3 >> _4 >> _5;
		}
		// Appends argument(s) into one string
		template <typename T1>
		static std::string toString(T1 _1)
		{
			std::stringstream stream;
			stream << _1;
			return stream.str();
		}
		// Appends argument(s) into one string
		template <typename T1, typename T2>
		static std::string toString(T1 _1, T2 _2)
		{
			std::stringstream stream;
			stream << _1 << _2;
			return stream.str();
		}
		// Appends argument(s) into one string
		template <typename T1, typename T2, typename T3>
		static std::string toString(T1 _1, T2 _2, T3 _3)
		{
			std::stringstream stream;
			stream << _1 << _2 << _3;
			return stream.str();
		}
		// Appends argument(s) into one string
		template <typename T1, typename T2, typename T3, typename T4>
		static std::string toString(T1 _1, T2 _2, T3 _3, T4 _4)
		{
			std::stringstream stream;
			stream << _1 << _2 << _3 << _4;
			return stream.str();
		}
		// Appends argument(s) into one string
		template <typename T1, typename T2, typename T3, typename T4, typename T5>
		static std::string toString(T1 _1, T2 _2, T3 _3, T4 _4, T5 _5)
		{
			std::stringstream stream;
			stream << _1 << _2 << _3 << _4 << _5;
			return stream.str();
		}
	};

}
