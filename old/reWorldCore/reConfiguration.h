#pragma once
#include <string>
#include "reSingleton.h"
namespace reWorldCore
{
	class reXMLNode;
	class reConfiguration : public reSingleton<reConfiguration>
	{
	private:
		reXMLNode* mConfigurationXML;
	public:
		reConfiguration(void)
		{

		}
		~reConfiguration(void)
		{

		}
		void initialise(std::string configurationFile = "configuration.xml");
		std::string config(std::string configName) const;

	};

}