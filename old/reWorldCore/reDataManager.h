#pragma once
#include "reXMLParser.h"
#include "reStringTools.h"
#include "reSingleton.h"
#include "reConfiguration.h"
#include <vector>

namespace reWorldCore
{
	template <typename T>
	class reDataManager : public reSingleton<reDataManager<T> >
	{
	private:
		std::vector<T*> mData;
	protected:
		reDataManager()
		{

		}
	public:
		void initialise(const std::string& itemName)
		{
			std::string filename = reConfiguration::getInstance().config(itemName);
			reXMLNode* node = reXMLParser::parseFile(filename);
			const reXMLNode* subnode = node->node(itemName);
			for(auto it=subnode->nodesBegin(), end=subnode->nodesEnd(); it!=end; ++it)
			{
				T* data = new T();
				data->parseXML(subnode);
				mData.push_back(data);
			}
		}
		const T* data(std::string name) const
		{
			for(auto it=mData.cbegin(), end=mData.cend(); it!=end; ++it)
			{
				if((*it)->name() == name)
				{
					return *it;
				}
			}
			return nullptr;
		}
	};

}